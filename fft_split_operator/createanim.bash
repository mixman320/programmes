#! /bin/bash
let x=1
rm  createanim.gp

while [ -e "tso0${x}0.dat" ] ; do 
  datf="tso0${x}0.dat"
  giff="tso0${x}0.gif"
  echo 'tso'0$x0'.dat' 
  echo 'set terminal gif' >> createanim.gp
  echo "set output '$giff'" >> createanim.gp
  echo "plot [-5:5] [0:5] '$datf' w l" >> createanim.gp
  LIST="$LIST $giff"
  
  let x=$x+1
done
while [ -e "tso${x}0.dat" ] ; do 
  datf="tso${x}0.dat"
  giff="tso${x}0.gif"
  echo 'tso'$x0'.dat' 
  echo 'set terminal gif' >> createanim.gp
  echo "set output '$giff'" >> createanim.gp
  echo "plot [-5:5] [0:5] '$datf' w l" >> createanim.gp
  LIST="$LIST $giff"
  let x=$x+1
done
gnuplot  createanim.gp
convert t -delay 6 $LIST -loop 0 animated.gif 
