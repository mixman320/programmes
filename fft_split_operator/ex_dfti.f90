!*****************************************************************************
! Copyright(C) 2011-2014 Intel Corporation. All Rights Reserved.
!
! The source code, information  and  material ("Material") contained herein is
! owned  by Intel Corporation or its suppliers or licensors, and title to such
! Material remains  with Intel Corporation  or its suppliers or licensors. The
! Material  contains proprietary information  of  Intel or  its  suppliers and
! licensors. The  Material is protected by worldwide copyright laws and treaty
! provisions. No  part  of  the  Material  may  be  used,  copied, reproduced,
! modified, published, uploaded, posted, transmitted, distributed or disclosed
! in any way  without Intel's  prior  express written  permission. No  license
! under  any patent, copyright  or  other intellectual property rights  in the
! Material  is  granted  to  or  conferred  upon  you,  either  expressly,  by
! implication, inducement,  estoppel or  otherwise.  Any  license  under  such
! intellectual  property  rights must  be express  and  approved  by  Intel in
! writing.
!
! *Third Party trademarks are the property of their respective owners.
!
! Unless otherwise  agreed  by Intel  in writing, you may not remove  or alter
! this  notice or  any other notice embedded  in Materials by Intel or Intel's
! suppliers or licensors in any way.
!
!*****************************************************************************
! Content:
! A simple example of double-precision complex-to-complex in-place 1D
! FFT using MKL DFTI
!
!*****************************************************************************

program basic_dp_complex_dft_1d

  use MKL_DFTI, forget => DFTI_DOUBLE, DFTI_DOUBLE => DFTI_DOUBLE_R

  ! Size of 1D transform
  integer, parameter :: N = 2048

  ! Working precision is double precision
  integer, parameter :: WP = selected_real_kind(15,307)

  ! Execution status
  integer :: status = 0, ignored_status

  ! The data array
  complex(WP), allocatable :: x (:)
  real*8, allocatable :: p(:)

  ! DFTI descriptor handle
  type(DFTI_DESCRIPTOR), POINTER :: hand
  real*8 :: alpha,norm,xmin,xmax,x0,dx,hermite,dp,pi

  hand => null()

  print *,"Example basic_dp_complex_dft_1d"
  print *,"Forward and backward double-precision complex-to-complex",        &
      " in-place 1D transform"
  print *,"Configuration parameters:"
  print *,"DFTI_PRECISION      = DFTI_DOUBLE"
  print *,"DFTI_FORWARD_DOMAIN = DFTI_COMPLEX"
  print *,"DFTI_DIMENSION      = 1"
  print '(" DFTI_LENGTHS        = /"I0"/" )', N

  print *,"Create DFTI descriptor"
  status = DftiCreateDescriptor(hand, DFTI_DOUBLE, DFTI_COMPLEX, 1, N)
  if (0 /= status) goto 999

  print *,"Commit DFTI descriptor"
  status = DftiCommitDescriptor(hand)
  if (0 /= status) goto 999

  print *,"Allocate array for input data"
  allocate (x(N))
  allocate (p(N))

  print *,"Initialize input for forward transform"
  xmin=-5.d0
  xmax=5.d0
  x0=0.d0
  alpha=1.D3


  norm=0.d0
  dx=(xmax-xmin)/dfloat(N-1)
  pi=dacos(-1.d0)
  write(*,*) pi
  dp=2.d0*pi/dx/N
  write(*,*) dp,N/2*dp
!paquet d'ondes à l'état initial (ici on a une gaussienne ordinaire)
  do i=1,N
    x(i)=dexp(-(dsqrt(alpha)*(xmin+(i-1)*dx))**2.d0/2.d0) 
    norm=norm+dabs(x(i))**2
  enddo
  norm=norm*dx
  x=x/dsqrt(norm)
  do i=1,N
    write(32,*) xmin+(i-1)*dx,abs(x(i))**2.d0
  enddo
!fin de la modification
  do l=1,9
    print *,"Compute forward transform"
    status = DftiComputeForward(hand, x)
    if (0 /= status) goto 999
    !write(*,*) 'dp',dp,'nst',N
    do k=N/2+2,N
      p(k)=(k-N-1)*dp
      write(100*l+33,*) p(k),abs(x(k))**2.d0
    end do
    do k=1,N/2+1            
      p(k)=(k-1)*dp 
      write(100*l+33,*) p(k),abs(x(k))**2.d0
    end do


    print *,"Compute backward transform"
    status = DftiComputeBackward(hand, x)
    if (0 /= status) goto 999
    x=x/N
    do i=1,N
      write(100*l+34,*) xmin+(i-1)*dx,abs(x(i))**2.d0
    end do

  end do
  100 continue

  print *,"Release the DFTI descriptor"
  ignored_status = DftiFreeDescriptor(hand)

  if (allocated(x)) then
    print *,"Deallocate data array"
    deallocate(x)
  endif

  if (status == 0) then
    print *,"TEST PASSED"
    call exit(0)
  else
    print *,"TEST FAILED"
    call exit(1)
  endif

  999 print '("  Error, status = ",I0)', status
  goto 100




end program basic_dp_complex_dft_1d
