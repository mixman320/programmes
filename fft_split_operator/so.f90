program so

  use MKL_DFTI
implicit none
  ! Size of 1D transform
  integer, parameter :: N = 2048
  ! Working precision is double precision
  integer, parameter :: WP = selected_real_kind(15,307)
  ! Execution status
  integer :: status = 0, ignored_status
  ! The data array
  complex(WP), allocatable :: psi (:,:)
	complex*16::i
  real*8, allocatable :: p(:)
  ! DFTI descriptor handle
  type(DFTI_DESCRIPTOR), POINTER :: hand
	! 
  real*8 :: norm,xmin,xmax,x0,dx,dp,pi,kmin,dt,m,w,eo,ti,tf
	integer::y,z,t,ntf
	character(50)::tso

  hand => null()

  print *,"Create DFTI descriptor"
  status = DftiCreateDescriptor(hand, DFTI_DOUBLE, DFTI_COMPLEX, 1, N)
  if (0 /= status) goto 999

  print *,"Commit DFTI descriptor"
  status = DftiCommitDescriptor(hand)
  if (0 /= status) goto 999

  print *,"Initialize input for forward transform"
	ti=0.d0
	tf=1.d0
	ntf=999
  xmin=-5.d0
  xmax=5.d0
  x0=0.d0
	i=(0.d0,1.d0)
  norm=0.d0
  dx=(xmax-xmin)/dfloat(N-1)
  pi=dacos(-1.d0)
  dp=2.d0*pi/dx/N
	dt=(tf-ti)/dfloat(ntf-1)
	kmin=(-N*dp/2.d0)
	m=918.d0
	w=0.0201d0
	eo=0.1d0

  print *,"Allocate array for input data"
  allocate (psi(N,0:ntf))
  allocate (p(N))

print *,'préparation du paquet d''ondes initial'
!paquet d'ondes à l'état initial (ici on a une gaussienne ordinaire)
t=0
  do y=1,N
    psi(y,t)=dexp(-(dsqrt(m*w)*(xmin+(y-1)*dx))**2.d0/2.d0) 
    norm=norm+(cdabs(psi(y,t))**2)*dx
  enddo
  psi=psi/dsqrt(norm)
	open (32,file='paquetinitial.dat',status='unknown')
  do y=1,N
    write(32,*) xmin+(y-1)*dx,cdabs(psi(y,t))**2.d0
  enddo
	close (32)
print *,'le bout tough'
!time loop
t=1
do
!partie 1 du split operator
	do y=1,N
		psi(y,t)=psi(y,t-1)*cdexp((-i*(m*(w**2.d0)*(xmin+(y-1)*dx)**2)+(xmin+(y-1)*dx)*eo*dcos(w*(dfloat(t)*dt-dt)))*dt/2.d0)
	end do
  !FFT
		!open (33)
    print *,"Compute forward transform",t
    status = DftiComputeForward(hand, psi(:,t))
    if (0 /= status) goto 999
    do z=N/2+2,N
      p(z)=(z-N-1)*dp
      !write(33,*) p(z),cdabs(psi(z,t))**2.d0
    end do
    do z=1,N/2+1            
      p(z)=(z-1)*dp 
      !write(33,*) p(z),cdabs(psi(z,t))**2.d0
    end do
		!close (33)
	!Fin FFT
!partie 2 du split operator
	do z=1,N
		psi(z,t)=psi(z,t)*cdexp(-i*(kmin+(z-1)*dp)**2.d0*dt/(m*2.d0))
	end do
	!FFTI
    print *,"Compute backward transform"
    status = DftiComputeBackward(hand, psi(:,t))
    if (0 /= status) goto 999
    psi=psi/N			!<-ça sert à quoi cette partie là?-/-/-/-/-/-/-/-/-/-/-/-/-/
    !do i=1,N
      !write(100*l+34,*) xmin+(y-1)*dx,cdabs(psi(y,t))**2.d0
    !end do
	!Fin FFTI
!partie 3 du split operator
write(tso,'(A3,I3.3,A4)') 'tso',t,'.dat'
open(23,file=tso,status='unknown')
	do y=1,N
		psi(y,t)=psi(y,t)*cdexp((-i*(m*(w**2.d0)*(xmin+(y-1)*dx)**2)+(xmin+(y-1)*dx)*eo*dcos(w*(dfloat(t)*dt-dt)))*dt/2.d0)
	write (23,*) xmin+(y-1)*dx,cdabs(psi(y,t))**2.d0
	end do
	close(23)
	if (t>=ntf) exit
	t=t+1
end do
  !finale de gossage
  100 continue

  print *,"Release the DFTI descriptor"
  ignored_status = DftiFreeDescriptor(hand)

  if (allocated(psi)) then
    print *,"Deallocate data array"
    deallocate(psi)
  endif

  if (status == 0) then
    print *,"TEST PASSED"
    call exit(0)
  else
    print *,"TEST FAILED"
    call exit(1)
  endif

  999 print '("  Error, status = ",I0)', status
  goto 100
	!fin de la finale

end program so
