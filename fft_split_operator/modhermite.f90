! pour la compilation, utilisez : gfortran hermite.f90 testhermite.f90 -o testhermite
module modhermite
implicit none
contains
!calcul de polynômes d'hermite
	subroutine subhermite (her,x,xmin,dx,hermite,j,n,nmax,a,xo,npts)
    	real*8 ::hermite,dx,xmin,a,xo
	real*8,intent(out)::her(npts,0:nmax),x(npts)
    	integer, intent(in) :: j,n,nmax,npts
    	!do n=0,nmax
        	!do j=1,1024
            		x(j)=xmin+(j-1)*dx
            		her(j,n)=hermite(n,xo,a,x(j))
        	!end do
    	!end do
    	!do j=1,1024
        	!write(13,'(7E23.6)') x(j),(her(j,n),n=0,nmax)
    	!end do

	end subroutine subhermite

end module modhermite
