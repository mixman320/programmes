program main
!->load external procedures and data<-
use psi_osc_module
use grid_module
use time_module
implicit none
!->declarations<-
 character(50)::parameters,coefficients,grid_par,time_par
type(psi_osc)::Psi
type(grid_desc)::grid
type(time_desc)::time
!->calls<-
parameters="parameters.nml"
coefficients="coefficient.nml"
grid_par="grid.nml"
time_par="time.nml"
call Psi%import(parameters)
!call Psi%echo_param()
call grid%x_grid(grid_par)
call Psi%init_state(grid)
call time%t_grid(time_par)
call Psi%wp(grid,time,coefficients)
call Psi%echo_param()
!call Psi%prepare_dynamics()
!call Psi%dynamics()
end program
