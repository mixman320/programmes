module time_module
!->time object<-
implicit none
private
public::time_desc

namelist/time_nml/ti,tf,tpts
	Real*8,private::ti
	Real*8,private::tf
	Integer*4,private::tpts

	type::time_desc
		Real*8,private::ti
		Real*8,private::tf
		Integer*4::tpts
		Real*8::dt
		Real*8,dimension(:),allocatable::t(:)
	contains
		procedure,public::t_grid=>t_grid_vector
	end type

contains
!->routine that divides the time range into grid points<-
	subroutine t_grid_vector(self,time_par)
		class(time_desc),intent(inout)::self
		Character(*)::time_par
		Integer::j
		open(15,file=time_par,status="old")
		read(15,nml=time_nml)
		close(15)
		self%ti=ti
		self%tf=tf
		self%tpts=tpts
		self%dt=(tf-ti)/dfloat(tpts-1)
		allocate(self%t(tpts))
		do j=1,tpts
			self%t(j)=ti+(j-1)*self%dt
		end do
	end subroutine

end module time_module
