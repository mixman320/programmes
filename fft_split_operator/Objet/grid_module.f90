module grid_module
!->grid object <-
implicit none
private
public::grid_desc

namelist/grid_nml/xmin,xmax,xpts
	Real*8,private::xmin
	Real*8,private::xmax
	integer*4,private::xpts

	type::grid_desc
		Real*8,private::xmin
		Real*8,private::xmax
		Integer*4::xpts
		Real*8::dx
		Real*8,dimension(:),allocatable::x(:)
	contains
		procedure,public::x_grid=>x_grid_vector
	end type

contains
!->routine that divides the range into grid points<-
	subroutine x_grid_vector(self,grid_par)
		class(grid_desc),intent(inout)::self
		Character(*)::grid_par
		Integer::j
		open(13,file=grid_par,status='old')
		read(13,nml=grid_nml)
		close(13)
		self%xmin=xmin
		self%xmax=xmax
		self%xpts=xpts
		self%dx=(xmax-xmin)/(xpts-1)
		allocate(self%x(xpts))
		do j=1,xpts
			self%x(j)=xmin+(j-1)*self%dx
		end do
	end subroutine	

end module grid_module
