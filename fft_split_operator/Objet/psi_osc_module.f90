module psi_osc_module
use grid_module
use time_module
use MKL_DFTI
!->wave packet object<-
implicit none
private
public::psi_osc

	namelist/parameters_nml/Mass,System_frequency,Laser_frequency,Max_energy,nmax
	Real*8,private::Mass
	Real*8,private::System_frequency
	Real*8,private::Laser_frequency
	Real*8,private::Max_energy
	Integer*4,private::nmax
	

	type::psi_osc
		Real*8,private::Mass
		Real*8,private::System_frequency
		Real*8,private::Laser_frequency
		Real*8,private::Max_energy
		Integer*4,private::nmax
		Real*8,private::alpha
		Real*8,dimension(:,:),allocatable,private::phi(:,:)
		Real*8,dimension(:),allocatable,private::en(:)
		Real*8,dimension(:),allocatable,private::coeff(:)
		complex*16,dimension(:,:),allocatable,private::psi(:,:)
	contains
		procedure,public::import=>import_everything
		procedure,public::echo_param=>echo_psi_osc_parameters
		procedure,public::init_state=>get_initial_state
		procedure,public,nopass::fact=>factorial
		procedure,public,nopass::hermite
		procedure,public::wp=>initial_wave_packet
	end type

contains
!->routine that reads in the parameters of the wave packet<-
	subroutine import_everything(self,parameters)
		class(psi_osc),intent(inout)::self
		character(*)::parameters
		open(11,file=parameters,status='old')
		read(11,nml=parameters_nml)
		close(11)
		self%Mass=Mass
		self%System_frequency=System_frequency
		self%Laser_frequency=Laser_frequency
		self%Max_energy=Max_energy
		self%nmax=nmax
		self%alpha=System_frequency*Mass
	end subroutine
!->routine that returns the parameters read<-
	subroutine echo_psi_osc_parameters(self)
		class(psi_osc),intent(inout)::self
		Integer::n
		write(*,*) "System Parameters"
		write(*,"(T6,A,F5.1)") "m= ",self%Mass
		write(*,"(T6,A,ES8.2E1)") "ws=",self%System_frequency
		write(*,"(T6,A,ES8.2E1)") "wl=",self%Laser_frequency
		write(*,"(T6,A,F5.2)") "E0=",self%Max_energy
		write(*,"(T6,A,I1)") "nmax= ",self%nmax
		write(*,"(T6,A,F7.4)") "alpha= ",self%alpha
!		write(*,*) (self%coeff(n),n=0,self%nmax)
	end subroutine
!->routine that computes the stationary states and their energy<-
	subroutine get_initial_state(self,grid)
		class(psi_osc),intent(inout)::self
		class(grid_desc),intent(in)::grid
		real*8::hermite,her
		integer::n,j
		real*8,parameter::pi=dacos(-1.d0)
		allocate(self%phi(grid%xpts,0:self%nmax))
		allocate(self%en(0:self%nmax))
		!open(14,file='phi.dat',status='unknown')
		do n=0,self%nmax
			do j=1,grid%xpts
				her=self%hermite(n,self%alpha,grid%x(j))
				self%phi(j,n)=(self%alpha/pi)**(1.d0/4.d0)/sqrt(2.d0**n*dfloat(self%fact(n)))*(-1.d0)**n*her*dexp(-self%alpha/2.d0*grid%x(j)**2.d0)
			!write (14,*) grid%x(j),self%phi(j,n)
			end do
			self%en(n)=(n+0.5d0)*self%System_frequency
		end do
		!close(14)
	end subroutine get_initial_state

!->function that computes a factorial number<-
	recursive integer function factorial(n) result(fact)
	integer::n
	if (n==0) then
	fact=1
	else
	fact = n * factorial(n-1)
	endif
	end function factorial
!->function that computes the hermite polynomials<-
	real*8 function hermite(n,a,x)
	implicit none
	integer, intent(in) :: n
	real*8, intent(in) :: x,a
	integer::i
	real*8::h1,h2,ht
	if (n.eq.0) then
	  hermite=1.d0
	  return
	end if
	if (n.eq.1) then
	  hermite=2.d0*sqrt(a)*x
	  return
	end if
	h1=2.d0*sqrt(a)*x
	h2=1.d0
	do i=2,n
	  ht=2.d0*sqrt(a)*x*h1-2.d0*dble(i-1)*h2
	  h2=h1
	  h1=ht
	end do
	hermite=h1
	return
	end function hermite

!->routine that prepares the initial wave packet
	subroutine initial_wave_packet (self,grid,time,coefficients)
		class(psi_osc),intent(inout)::self
		class(grid_desc),intent(in)::grid
		class(time_desc),intent(in)::time
		integer*4::n,j
		real*8::wp_area,norm_wp
		Real*8,dimension(:),allocatable::coeff
		character(*)::coefficients
!		complex*16::i
!		i=(0.d0,1.d0)
		wp_area=0
		norm_wp=0
		allocate(coeff(0:self%nmax))
		namelist/coefficient_nml/coeff
		coeff=0
		open(12,file=coefficients,status='old')
		read(12,nml=coefficient_nml)
		close(12)
		allocate(self%coeff(0:self%nmax))
		allocate(self%psi(grid%xpts,time%tpts))
		do n=0,self%nmax
			self%coeff(n)=coeff(n)
		end do
		self%psi=(0.d0,0.d0)
		do n=0,self%nmax
			do j=1,grid%xpts
				self%psi(j,1)=self%psi(j,1)+self%coeff(n)*self%phi(j,n)
			end do
		end do
		do j=1,grid%xpts-1
			wp_area=wp_area+(cdabs(self%psi(j+1,1)+self%psi(j,1))/2.d0)**2.d0*grid%dx
		end do
		norm_wp=1.d0/dsqrt(wp_area)
		self%psi(:,1)=self%psi(:,1)*norm_wp
		open(40,file="wpo0000.dat",status="unknown")
		do j=1,grid%xpts
			write(40,*) grid%x(j),cdabs(self%psi(j,1))**2.d0
		end do
		close (45)
	end subroutine initial_wave_packet

end module psi_osc_module
