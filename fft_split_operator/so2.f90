!Programme qui utilise le split-operator pour représenter la dynamique d'un paquet d'ondes v2.1
program so2
use modhermite
use MKL_DFTI
implicit none
interface
	real(8) function hermite(n,xo,a,x,npts)
		integer, intent(in) :: n,npts
		real(8), intent(in) :: xo,a
		real*8, dimension (1:npts):: x
	end function hermite
end interface
!----------------déclaration de variables-----------------------
real*8::xmax,xmin,xo,dx,m,w,a,aire,dt,airewp,xmoy,pmoy,pi,dp,tf,ti,eo,normwp,aireft
integer::j,npts,n,nmax,t,k,ntf
real*8, dimension (:), allocatable:: x,norm,en,d
real*8, dimension (:,:), allocatable::phi,her,pot,pn
complex*16::i

 character(1)::ok
 character(9)::fname,potnm,wp,fftn
 character(10)::tso
!partie fourier
integer, parameter :: cp = selected_real_kind(15,307)
integer :: status = 0, ignored_status
complex(cp), dimension (:,:), allocatable::psi
real*8, allocatable :: p(:)
type(DFTI_DESCRIPTOR), POINTER :: hand
!----------------initialisation de variables------------------------
write (*,*) 'Initialisation des variables'
xmax=5.d0
xmin=-5.d0
xo=0.d0
npts=2048
dx=(xmax-xmin)/dfloat(npts-1)
m=918.d0 
w=0.0201d0 
a=m*w
ti=0.d0
ntf=990
n=0
i=(0.d0,1.d0)
pi=dacos(-1.d0)
dp=2.d0*pi/dx/npts
eo=0.1d0
hand => null()
 print *,"Create DFTI descriptor"
  status = DftiCreateDescriptor(hand, DFTI_DOUBLE, DFTI_COMPLEX, 1, npts)
  if (0 /= status) goto 999
 print *,"Commit DFTI descriptor"
  status = DftiCommitDescriptor(hand)
  if (0 /= status) goto 999
!----------------lecture des paramètres------------------------------
write (*,*) 'Lecture des paramètres'
do
	write (*,*) 'entrez les entiers nmax et tf'
	read (*,*) nmax
	read (*,*) tf
	allocate (d(0:nmax))
	do n=0,nmax
		write (*,'(A23,I2.1,A8)') 'entrez la valeur de d(',n,') [0..1]'
		read (*,*) d(n)
	end do
	dt=(tf-ti)/dfloat(ntf-1)
	write (*,'(A5,I2.1,A4,F6.1,A4,F8.5)')  'nmax=',nmax,' tf=',tf, ' dt=',dt
	do n=0,nmax
		write (*,'(A2,I2.1,A2)') 'd(',n,')='
		write (*,'(F9.6)') d(n)
	end do
	write (*,'(A41)') 'ces paramètres sont-ils corrects? [y/n]'
	read (*,*) ok
	if (ok == 'y') exit
	write (*,*) ok,', essayez de nouveau'
end do

!----------------allocation des vecteurs----------------------------
write(*,*) 'Allocation des vecteurs'
allocate(her(npts,0:nmax))
allocate(norm(0:nmax))
allocate(x(npts))
allocate(phi(npts,0:nmax))
allocate(psi(npts,0:ntf))
allocate(en(0:nmax))
allocate(p(npts))
allocate(pot(npts,0:ntf))
allocate(pn(0:nmax,0:ntf))

!----------------------calcul des états stationnaires---------------------
write(*,*) 'Calcul des états stationnaires + de la constante de normalisation + la dite normalisation + les niveaux d''énergie'
do n=0,nmax
	do j=1,npts !états stationnaires
		call subhermite (her,x,xmin,dx,hermite,j,n,nmax,a,xo,npts)
		phi(j,n)=(-1.d0)**n*her(j,n)*dexp(-a/2.d0*(x(j)-xo)**2.d0)
	end do
	aire=0
	do j=1,npts-1 !constante de normalisation
		aire=aire+(dabs(phi(j+1,n)+phi(j,n))/2.d0)**2.d0*dx
	end do
	norm(n)=1.d0/dsqrt(aire)
	do j=1,npts
		phi(j,n)=phi(j,n)*norm(n) !normé
	end do
	en(n)=(n+0.5d0)*w !niveaux d'énergie
end do

!--------------------calcul du paquet d'ondes-----------------------------
write(*,*) 'Calcul du paquet d''ondes'
psi=(0.d0,0.d0)
airewp=0
t=0
	do n=0,nmax
		do j=1,npts
			psi(j,t)=psi(j,t)+d(n)*cdexp(-i*en(n)*dfloat(t)*dt)*phi(j,n)
		end do
	end do
!------------------normalisation du paquet d'ondes--------------------------
write(*,*) 'Calcul de la normalisation'
	do j=1,npts-1
		airewp=airewp+(cdabs(psi(j+1,t)+psi(j,t))/2.d0)**2.d0*dx
	end do
	normwp=1.d0/dsqrt(airewp)
!--------------------paquet d'ondes normalisé-----------------------------
write(*,*) 'Normalisation'
	psi(:,0)=psi(:,0)*normwp
open(45,file='tso000.dat',status='unknown')
do j=1,npts
write(45,*) x(j),cdabs(psi(j,t))**2.d0
end do
 close(45)
!------------------time loop+init. de paramètres pour px------------------
t=1
do
pmoy=0.d0
aireft=0.d0
open(63,file='pmoy.dat',status='unknown')
!---------------------partie 1 du split operator----------------------------
	do j=1,npts
		psi(j,t)=psi(j,t-1)*cdexp(-i*v(m,w,eo,j,xmin,dx,x,t,dt)*dt/2.d0)
	end do
	!FFT	
    print *,"Compute forward transform",t
    status = DftiComputeForward(hand, psi(:,t))
    if (0 /= status) goto 999
    do k=npts/2+2,npts
      p(k)=(k-npts-1)*dp
    end do
    do k=1,npts/2+1            
      p(k)=(k-1)*dp 
    end do
	!Fin FFT
!------------------partie 2 du split operator-------------------------
	do k=1,npts
		psi(k,t)=psi(k,t)*cdexp(-i*p(k)**2.d0*dt/(m*2.d0))
		aireft=aireft+(cdabs(psi(k,t)))**2.d0*dp
	end do
	do k=1,npts	!parenthèse calcul de px
		pmoy=pmoy+(p(k)*cdabs(psi(k,t)/dsqrt(aireft))**2.d0)*dp
	end do
	write(63,*)t,pmoy
	!FFTI
    print *,"Compute backward transform"
    status = DftiComputeBackward(hand, psi(:,t))
    if (0 /= status) goto 999
    psi(:,t)=psi(:,t)/npts			!<-renormalisation
	!Fin FFTI
!----------------------partie 3 du split operator---------------------
	do j=1,npts
		psi(j,t)=psi(j,t)*cdexp(-i*v(m,w,eo,j,xmin,dx,x,t,dt)*dt/2.d0)
		if (mod(t,10)==0) then
			write(tso,'(A3,I3.3,A4)') 'tso',t,'.dat'
			open(23,file=tso,status='unknown')
			write (23,*) x(j),cdabs(psi(j,t))**2.d0
		end if
	end do
	close(23)
	if (t>=ntf) exit
	t=t+1
end do
 close (63)
!----------------------------xmoyen----------------------------------
open(62,file='xmoy.dat',status='unknown')
t=0
do
xmoy=0.d0
	do j=1,npts
		xmoy=xmoy+(x(j)*(cdabs(psi(j,t)))**2.d0)*dx
	end do
	write(62,*)t,xmoy
	if (t>=ntf) exit
	t=t+1
end do
 close(62)

!-----------------------------probabilités----------------------------
pn=0.d0
do t=0,ntf
	do n=0,nmax
		do j=1,npts
			pn(n,t)=pn(n,t)+cdabs(phi(j,n)*psi(j,t))**2.d0*dx
		end do
	end do
	!write (56,*) (pn(n,t),n=0,nmax)
end do

  !finale de gossage---------------------------------------------------
  100 continue

  print *,"Release the DFTI descriptor"
  ignored_status = DftiFreeDescriptor(hand)

  if (allocated(psi)) then
    print *,"Deallocate data array"
    deallocate(psi)
  endif

  if (status == 0) then
    print *,"TEST PASSED"
    call exit(0)
  else
    print *,"TEST FAILED"
    call exit(1)
  endif

  999 print '("  Error, status = ",I0)', status
  goto 100
	!fin de la finale
!------------------------fonction potentiel/opérateur V-----------------
contains

real*8 function v(m,w,eo,j,xmin,dx,x,t,dt)
implicit none
	integer::j,t
	real*8::m,w,eo,dt,dx,xmin,x(j)
	x(j)=xmin+(j-1)*dx
	pot(j,t)=(m*(w**2.d0)*x(j)**2)+x(j)*eo*dcos(w*(dfloat(t)*dt-dt))
	v=pot(j,t)
return
end function v


end program so2
