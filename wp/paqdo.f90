program paqdo
use modhermite
use MKL_DFTI
implicit none
interface
	real(8) function hermite(n,xo,a,x,npts)
		integer, intent(in) :: n,npts
		real(8), intent(in) :: xo,a
		real*8, dimension (1:npts):: x
		! local variables
		integer i
		real(8) h1,h2,ht
	end function hermite
end interface
!déclaration de variables
real*8::xmax,xmin,xo,dx,m,w,a,aire,dt,airewp,xmoy,pi,dp,pmoy,aireft
integer::j,npts,n,nmax,t,tf,k
real*8, dimension (:), allocatable:: x,norm,en,d,normwp
real*8, dimension (:,:), allocatable::phi,her
complex*16::i

 character(1)::ok
 character(9)::fname,potnm,wp,fftn
!partie fourier
integer, parameter :: cp = selected_real_kind(15,307)
integer :: status = 0, ignored_status
complex(cp), dimension (:,:), allocatable::psi
real*8, allocatable :: p(:)
type(DFTI_DESCRIPTOR), POINTER :: hand
!initialisation de variables
xmax=5.d0
xmin=-5.d0
xo=0.d0
npts=1024
dx=(xmax-xmin)/dfloat(npts-1)
m=918.d0 
w=0.0201d0 
a=m*w
dt=1.d0/w
n=0
i=(0.d0,1.d0)
pi=dacos(-1.d0)
hand => null()
dp=2.d0*pi/dx/npts
!lecture des paramètres
do
	write (*,*) 'entrez les entiers nmax et tf'
	read (*,*) nmax
	read (*,*) tf
	allocate (d(0:nmax))
	do n=0,nmax
		write (*,'(A23,I2.1,A8)') 'entrez la valeur de d(',n,') [0..1]'
		read (*,*) d(n)
	end do
	write (*,'(A5,I2.1,A4,I3.1)')  'nmax=',nmax,' tf=',tf
	do n=0,nmax
		write (*,'(A2,I2.1,A2)') 'd(',n,')='
		write (*,'(F9.6)') d(n)
	end do
	write (*,'(A40)') 'ces paramètres sont-ils corrects? [y/n]'
	read (*,*) ok
	if (ok == 'y') exit
	write (*,*) ok,', essayez de nouveau'
end do

!allocation des vecteurs
write(*,*) 'Allocation des vecteurs'
allocate(her(npts,0:nmax))
allocate(norm(0:nmax))
allocate(x(npts))
allocate(phi(npts,0:nmax))
allocate(psi(npts,0:tf))
allocate(en(0:nmax))
allocate(normwp(0:tf))
allocate(p(npts))

!calcul de phi
write(*,*) 'Calcul des états stationnaires'
do n=0,nmax
	do j=1,npts
		call subhermite (her,x,xmin,dx,hermite,j,n,nmax,a,xo,npts)!
		phi(j,n)=(-1.d0)**n*her(j,n)*dexp(-a/2.d0*(x(j)-xo)**2.d0)
		
	end do
end do

!calcul de la normalisation
write(*,*) 'Calcul de la constante de normalisation'
do n=0,nmax
	aire=0
	do j=1,npts-1
		aire=aire+(dabs(phi(j+1,n)+phi(j,n))/2.d0)**2.d0*dx
	end do
	norm(n)=1.d0/dsqrt(aire)
	!write (*,'(A37,I2.1,A5)') 'la constante de normalisation pour n=',n,' est:'
	!write (*,*) norm(n)
end do

!calcul de phi normalisé
write(*,*) 'Normalisation'
do n=0,nmax
	write(fname,'(A3,I2.2,A4)') 'phi',n,'.dat'
	open (13,file=fname,status='unknown')
	do j=1,npts
		phi(j,n)=phi(j,n)*norm(n)
		write (13,*) x(j),phi(j,n)
	end do
	close(13)
end do
!calcul de l'énergie
write(*,*) 'Détermination des niveaux d''énergie'
do n=0,nmax
	en(n)=(n+0.5d0)*w
	!write(*,'(A20,I2.1,A5)')'L''énergie du niveau',n,' est:'
	!write(*,*) en(n)
end do

!potentiel harmonique et ses états stationnaires
write(*,*) 'Écriture des états stationnaires dans le potentiel harmonique'
do n=0,nmax
	write (potnm,'(A3,I2.2,A4)')'pot',n,'.dat'
	open(15,file=potnm,status='unknown')
	do j=1,npts
		write (15,*) x(j),phi(j,n)+en(n)
	end do
	close(15)
end do

!calcul du PAQUET D'ONDES!
write(*,*) 'Calcul du paquet d''ondes'
psi=(0.d0,0.d0)

t=0
do
	do n=0,nmax
		do j=1,npts
			psi(j,t)=psi(j,t)+d(n)*cdexp(-i*en(n)*dfloat(t)*dt)*phi(j,n)
		end do
	end do
	if (t>=tf) exit
	t=t+1
end do
!normalisation du paquet d'ondes
write(*,*) 'Calcul de la normalisation'
t=0
do
	airewp=0
	do j=1,npts-1
		airewp=airewp+(cdabs(psi(j+1,t)+psi(j,t))/2.d0)**2.d0*dx
	end do
	normwp(t)=1.d0/dsqrt(airewp)
	!write (*,'(A37,I2.1,A5)') 'la constante de normalisation pour t=',t,' est:'
	!write (*,*) normwp(t)
	if (t>=tf) exit
	t=t+1
end do
!paquet d'ondes normalisé
write(*,*) 'Normalisation'
t=0
open (18,file='xmoy.dat',status='unknown')
do
	xmoy=0.d0
	write (wp,'(A3,I2.1,A4)')'wpt',t,'.dat'
	open (12,file=wp,status='unknown')
	do j=1,npts
		psi(j,t)=psi(j,t)*normwp(t)
		write (12,*) x(j),cdabs(psi(j,t))
		xmoy=xmoy+(x(j)*cdabs(psi(j,t))**2.d0)*dx
	end do
	write (18,*)t,xmoy
	if (t>=tf) exit
	t=t+1
	close(12)
end do
 close(18)
!Transformons de Fouriance avec un peu de trucs pour commencer
write(*,*) 'Transformée de Fourier'
print *,"Create DFTI descriptor"
  status = DftiCreateDescriptor(hand, DFTI_DOUBLE, DFTI_COMPLEX, 1, npts)
  if (0 /= status) goto 999

  print *,"Commit DFTI descriptor"
  status = DftiCommitDescriptor(hand)
  if (0 /= status) goto 999
!ce qui nous intéresse
t=0
open (19,file='pmoy.dat',status='unknown')
do
pmoy=0.d0
aireft=0.d0
write (fftn,'(A3,I2.1,A4)') 'fft',t,'.dat'
open(33,file=fftn,status='unknown')
 print '(A,I2.1)',"Compute forward transform, t=",t
    status = DftiComputeForward(hand, psi(:,t))
    if (0 /= status) goto 999
	do k=1,npts
		aireft=aireft+(cdabs(psi(k,t)))**2.d0*dp
	end do
	psi(:,t)=(psi(:,t)/dsqrt(aireft))
    do k=npts/2+2,npts
      p(k)=(k-npts-1)*dp
      write(33,*) p(k),abs(psi(k,t))**2.d0
    end do
    do k=1,npts/2+1            
      p(k)=(k-1)*dp 
      write(33,*) p(k),abs(psi(k,t))**2.d0
    end do
	do k=1,npts
		pmoy=pmoy+(p(k)*cdabs(psi(k,t))**2.d0)*dp
	end do
	write (19,*)t,pmoy
 close(33)
 if (t>=tf)exit
 t=t+1
end do
 close (19)
!plein d'autres trucs
   100 continue

  print *,"Release the DFTI descriptor"
  ignored_status = DftiFreeDescriptor(hand)

  if (allocated(x)) then
    print *,"Deallocate data array"
    deallocate(x)
  endif

  if (status == 0) then
    print *,"TEST PASSED"
    call exit(0)
  else
    print *,"TEST FAILED"
    call exit(1)
  endif

  999 print '("  Error, status = ",I0)', status
  goto 100

end program paqdo
