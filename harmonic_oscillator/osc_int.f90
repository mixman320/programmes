Module osc_int

use basics
Implicit None
Private
Public::norm_osc,osc_moment_p1



Contains

Real(kind=real_8) function norm_osc(n,b)
 Implicit None
 Integer(kind=int_4),intent(in)::n
Real(kind=real_8),intent(in)::b
 norm_osc= ((b/pi)**(1_real_8/4_real_8)) * (1_real_8 / sqrt( fact(n)*2**n))
end function

real(kind=real_8) function osc_moment_p1(n,b)
 Implicit None
Integer(kind=int_4),intent(in)::n
Real(kind=real_8),intent(in)::b
 

osc_moment_p1=(b**(-.5_real_8))/(2_real_8*sqrt(2.d0*(n+1.d0)))

end function

recursive function fact(n,x) result(res)
!Calcul la multifactorielle de n. Si x=1->n!, si x=2->n!!, ...
!warning: rapidly generate big numbers, be sure than precision is correctly set to ensure 
!         expected result.
!warning: unless result is an integer, the output value is a real.
integer(kind=int_4), intent(in) :: n
integer(kind=int_4), optional, intent(in) :: x
real(kind=real_8) :: res
integer(kind=int_4)::x_=1
if (present(x))   x_=x

if (n<=0) then
	res=1_real_8
else
	res=real(n*fact(n-x_,x_),kind=real_8)
endif
end function fact

   function binomial(n,k) result(res)
      Implicit None
      integer(kind=int_4),intent(in)::n,k
      real(kind=real_8)::res
      ! assume n>k
      If (n.lt.k) Stop "binomial function assumes (n k) with n>k"
      If(k==0_int_4) then
         res=1_int_8
      Else if(k==n)then
         res=1_int_8
       Else If (n-k.ge.k) then
         res=big_int_trunked_factorial(n-k+1,n)/big_int_factorial(k)
       else if(n-k.lt.k) then
         res=big_int_trunked_factorial(k+1,n)/big_int_factorial(n-k)
      Else        
       Stop "binomial function: no formule for this case"
      End if
   end function binomial
   recursive function big_int_factorial(n,x) result(res)
      integer(kind=int_4), intent(in) :: n
      integer(kind=int_4), optional, intent(in) :: x
      integer(kind=real_8) :: res
      integer(kind=int_4)::x_=1
      if (present(x))   x_=x

      if (n<=0) then
         res=1_real_8
      else
          res=int(n*big_int_factorial(n-x_,x_),kind=int_8)
      endif
   end function big_int_factorial

   function trunked_factorial(n,m) result(res)
   ! compute nx(n+1)x...m!
     integer(kind=int_4),intent(in)::n,m 
     real(kind= real_8)::res
     integer(kind=int_4):: i
     if (n.gt.m) Stop "In medys_GUGA: Error in the application of trunked_factorial function." 
     res=n
     Do i=n+1,m,1
        res=res*i
     End Do
   end function trunked_factorial

   function big_int_trunked_factorial(n,m) result(res)
   ! compute nx(n+1)x...m!
     integer(kind=int_4),intent(in)::n,m 
     integer(kind= int_8)::res
     integer(kind=int_8):: i,n_,m_
     if (n.gt.m) Stop "In medys_GUGA: Error in the application of big_int_trunked_factorial function." 
     n_=int(n,kind=int_8)
     m_=int(m,kind=int_8)

     res=n_
     Do i=n_+1,m_,1
        res=res*i
     End Do
   end function big_int_trunked_factorial
End module
