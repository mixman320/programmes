Program test_osc

use osc_int
use basics

Integer(kind=int_4)::v,nv
Real(kind=real_8)::b
Real(kind=real_8),allocatable,dimension(:)::psi,mu_p1

nv=12
b=1._real_8



allocate(psi(nv))
allocate(mu_p1(nv))

psi=0.d0
mu_p1=0.d0

do v=0,nv-1 
    mu_p1(v+1)= osc_moment_p1(v,b)
write(*,*) mu_p1(v+1)
end do





End Program
